{ config, open-acme, pkgs, ... }: {
  security = {
    acme.defaults.server = "http://localhost:8000";
    pki.certificates = [
      ''
        -----BEGIN CERTIFICATE-----
        MIIBkjCCATegAwIBAgIRALENOUZRhQ/UoXiJLuKF7OEwCgYIKoZIzj0EAwIwJzEl
        MCMGA1UEAxMcVW50cnVzdGVkIEhhcmQgQ29kZWQgUm9vdCBDQTAgFw03MDAxMDEw
        MDAwMDBaGA8yMTAwMTIzMTIzNTk1OVowJzElMCMGA1UEAxMcVW50cnVzdGVkIEhh
        cmQgQ29kZWQgUm9vdCBDQTBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABDbEhfiD
        2s+zY8j2TWqC5WU9fTZkKzoQi1FVWo0zQH1caclSIU/OQ+pfgEMQu+Y+De7L8em6
        aV2sd1OxMby/85ijQjBAMA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/
        MB0GA1UdDgQWBBSQXqckKAJhPKIcYDKjw5mkAoN8rjAKBggqhkjOPQQDAgNJADBG
        AiEA6fGrA3hD5l0jKVK1tIERZudWpazqovFsMO+krZjJdqICIQDrp7t6/P9KOWSm
        TtX0z1Qmoa+BWDJETGVHfKj8mCO1Cg==
        -----END CERTIFICATE-----
      ''
    ];
  };
  systemd.services.open-acme = {
    wantedBy = [ "multi-user.target" ];
    after = [ "network.target" ];
    serviceConfig = {
      ExecStart = "${open-acme.packages.x86_64-linux.default}/bin/open-acme";
    };
  };
}
