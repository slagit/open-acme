package main

import (
	"crypto/rand"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"regexp"
	"sync"
	"time"
)

var (
	regexen = make(map[string]*regexp.Regexp)
	relock  sync.Mutex
)

type AuthzResponse struct {
	Status     string     `json:"status"`
	Identifier Identifier `json:"identifier"`
	Challenges []string   `json:"challenges"`
}

type DirectoryResponse struct {
	NewNonce   string `json:"newNonce"`
	NewAccount string `json:"newAccount"`
	NewOrder   string `json:"newOrder"`
}

type AccountResponse struct {
	Status string `json:"status"`
	Orders string `json:"orders"`
}

type FinalizeRequest struct {
	CSR string `json:"csr"`
}

type JoseJson struct {
	Protected string `json:"protected"`
	Payload   string `json:"payload"`
	Signature string `json:"signature"`
}

type NewOrderRequest struct {
	Identifiers []Identifier `json:"identifiers"`
}

type Identifier struct {
	Type  string `json:"type"`
	Value string `json:"value"`
}

type Order struct {
	Status         string       `json:"status"`
	Identifiers    []Identifier `json:"identifiers"`
	Authorizations []string     `json:"authorizations"`
	Finalize       string       `json:"finalize"`
	Certificate    string       `json:"certificate,omitempty"`
}

type OrderState struct {
	Certificate []byte
	Id          string
	Identifiers []Identifier
}

func (s *OrderState) Order() *Order {
	o := &Order{
		Finalize:    fmt.Sprintf("http://localhost:8000/order/%s/finalize", s.Id),
		Identifiers: s.Identifiers,
	}
	if s.Certificate == nil {
		o.Status = "pending"
	} else {
		o.Status = "valid"
		o.Certificate = fmt.Sprintf("http://localhost:8000/order/%s/certificate", s.Id)
	}
	o.Authorizations = make([]string, 0, len(s.Identifiers))
	for _, id := range s.Identifiers {
		idBytes, err := json.Marshal(id)
		if err != nil {
			panic(err)
		}
		path := base64.RawURLEncoding.EncodeToString(idBytes)
		o.Authorizations = append(o.Authorizations, fmt.Sprintf("http://localhost:8000/authz/%s", path))
	}
	return o
}

type AcmeHandler struct {
	CertificateAuthority CertificateAuthority
	Orders               map[string]*OrderState
}

func (ah *AcmeHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var id string

	var h http.HandlerFunc

	p := r.URL.Path
	switch {
	case match(p, "/"):
		h = get(ah.directory)
	case match(p, "/new-account"):
		h = post(ah.newAccount)
	case match(p, "/new-nonce"):
		h = head(ah.newNonce)
	case match(p, "/new-order"):
		h = post(ah.newOrder)
	case match(p, "/authz/([^/]+)", &id):
		h = post(authzWidget{ah, id}.get)
	case match(p, "/order/([^/]+)/certificate", &id):
		h = post(orderWidget{ah, id}.certificate)
	case match(p, "/order/([^/]+)/finalize", &id):
		h = post(orderWidget{ah, id}.finalize)
	default:
		http.NotFound(w, r)
		return
	}
	h.ServeHTTP(w, r)
}

func (*AcmeHandler) directory(w http.ResponseWriter, r *http.Request) {
	resp, err := json.Marshal(DirectoryResponse{
		NewNonce:   "http://localhost:8000/new-nonce",
		NewAccount: "http://localhost:8000/new-account",
		NewOrder:   "http://localhost:8000/new-order",
	})
	if err != nil {
		panic(err)
	}
	w.Header().Set("content-type", "application/json")
	w.Write(resp)
}

func (*AcmeHandler) newAccount(w http.ResponseWriter, r *http.Request) {
	resp, err := json.Marshal(AccountResponse{
		Status: "valid",
		Orders: "http://localhost:8000/orders",
	})
	if err != nil {
		panic(err)
	}
	w.Header().Set("content-type", "application/json")
	w.Header().Set("location", "http://localhost:8000/account")
	addNonce(w)
	w.WriteHeader(http.StatusCreated)
	w.Write(resp)
}

func (*AcmeHandler) newNonce(w http.ResponseWriter, r *http.Request) {
	addNonce(w)
}

func (ah *AcmeHandler) newOrder(w http.ResponseWriter, r *http.Request) {
	var payload NewOrderRequest
	if err := readJoseJson(r, &payload); err != nil {
		panic(err)
	}

	state := &OrderState{
		Id:          generateId(),
		Identifiers: payload.Identifiers,
	}
	if ah.Orders == nil {
		ah.Orders = make(map[string]*OrderState)
	}
	ah.Orders[state.Id] = state

	resp, err := json.Marshal(state.Order())
	if err != nil {
		panic(err)
	}
	w.Header().Set("content-type", "application/json")
	w.Header().Set("location", fmt.Sprintf("http://localhost:8000/order/%s", state.Id))
	addNonce(w)
	w.WriteHeader(http.StatusCreated)
	w.Write(resp)
}

type authzWidget struct {
	ah *AcmeHandler
	id string
}

func (aw authzWidget) get(w http.ResponseWriter, r *http.Request) {
	var id Identifier
	if err := readB64Json(aw.id, &id); err != nil {
		panic(err)
	}
	resp, err := json.Marshal(AuthzResponse{
		Status:     "valid",
		Identifier: id,
		Challenges: []string{},
	})
	if err != nil {
		panic(err)
	}
	w.Header().Set("content-type", "application/json")
	w.Header().Set("location", fmt.Sprintf("http://localhost:8000/authz/%s", aw.id))
	w.Write(resp)
}

type orderWidget struct {
	ah *AcmeHandler
	id string
}

func (ow orderWidget) certificate(w http.ResponseWriter, r *http.Request) {
	state, ok := ow.ah.Orders[ow.id]
	if !ok {
		http.NotFound(w, r)
		return
	}

	w.Header().Set("content-type", "application/pem-certificate-chain")
	addNonce(w)
	if err := pem.Encode(w, &pem.Block{
		Type:  "CERTIFICATE",
		Bytes: state.Certificate,
	}); err != nil {
		panic(err)
	}
	if err := pem.Encode(w, &pem.Block{
		Type:  "CERTIFICATE",
		Bytes: ow.ah.CertificateAuthority.IntermediateCertificate.Raw,
	}); err != nil {
		panic(err)
	}
}

func (ow orderWidget) finalize(w http.ResponseWriter, r *http.Request) {
	state, ok := ow.ah.Orders[ow.id]
	if !ok {
		http.NotFound(w, r)
		return
	}

	var payload FinalizeRequest
	if err := readJoseJson(r, &payload); err != nil {
		panic(err)
	}
	csrBytes, err := base64.RawURLEncoding.DecodeString(payload.CSR)
	if err != nil {
		panic(err)
	}
	csr, err := x509.ParseCertificateRequest(csrBytes)
	if err != nil {
		panic(err)
	}
	dnsNames := make([]string, 0, len(state.Identifiers))
	for _, i := range state.Identifiers {
		dnsNames = append(dnsNames, i.Value)
	}
	state.Certificate, err = x509.CreateCertificate(rand.Reader, &x509.Certificate{
		BasicConstraintsValid: true,
		DNSNames:              dnsNames,
		ExtKeyUsage: []x509.ExtKeyUsage{
			x509.ExtKeyUsageClientAuth,
			x509.ExtKeyUsageServerAuth,
		},
		IsCA:         false,
		KeyUsage:     x509.KeyUsageDigitalSignature | x509.KeyUsageKeyEncipherment,
		NotAfter: time.Now().Add(90 * 24 * time.Hour),
		NotBefore: time.Now().Add(-1 * time.Hour),
		SerialNumber: generateSerial(rand.Reader),
		Subject: pkix.Name{
			CommonName: state.Identifiers[0].Value,
		},
	}, ow.ah.CertificateAuthority.IntermediateCertificate, csr.PublicKey, ow.ah.CertificateAuthority.IntermediateKey)
	if err != nil {
		panic(err)
	}

	resp, err := json.Marshal(state.Order())
	if err != nil {
		panic(err)
	}
	w.Header().Set("content-type", "application/json")
	w.Header().Set("location", fmt.Sprintf("http://localhost:8000/order/%s", state.Id))
	addNonce(w)
	w.Write(resp)
}

func addNonce(w http.ResponseWriter) {
	w.Header().Set("cache-control", "no-store")
	w.Header().Set("replay-nonce", "demo-nonce")
}

func generateId() string {
	buf := make([]byte, 8)
	if _, err := rand.Read(buf); err != nil {
		panic(err)
	}
	return base64.RawURLEncoding.EncodeToString(buf)
}

func readB64Json(body string, payload interface{}) error {
	payloadBytes, err := base64.RawURLEncoding.DecodeString(body)
	if err != nil {
		return err
	}
	return json.Unmarshal(payloadBytes, &payload)
}

func readJoseJson(r *http.Request, payload interface{}) error {
	bodyBytes, err := io.ReadAll(r.Body)
	if err != nil {
		return err
	}
	var body JoseJson
	if err := json.Unmarshal(bodyBytes, &body); err != nil {
		return err
	}
	return readB64Json(body.Payload, payload)
}

func main() {
	ca, err := NewCertificateAuthority()
	if err != nil {
		panic(err)
	}
	if err := pem.Encode(os.Stdout, &pem.Block{
		Type:  "CERTIFICATE",
		Bytes: ca.RootCertificate.Raw,
	}); err != nil {
		panic(err)
	}

	h := AcmeHandler{
		CertificateAuthority: ca,
	}
	log.Fatal(http.ListenAndServe(":8000", &h))
}
