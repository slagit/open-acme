package main

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/x509"
	"crypto/x509/pkix"
	"io"
	"math/big"
	"math/rand"
	"time"
)

type CertificateAuthority struct {
	RootCertificate         *x509.Certificate
	IntermediateCertificate *x509.Certificate
	IntermediateKey         *ecdsa.PrivateKey
}

func NewCertificateAuthority() (CertificateAuthority, error) {
	var retval CertificateAuthority

	r := rand.New(rand.NewSource(0))
	rootKey, err := ecdsa.GenerateKey(elliptic.P256(), r)
	if err != nil {
		return CertificateAuthority{}, err
	}

	rootTemplate := &x509.Certificate{
		BasicConstraintsValid: true,
		IsCA:                  true,
		KeyUsage:              x509.KeyUsageCertSign | x509.KeyUsageCRLSign,
		NotBefore:             time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC),
		NotAfter:              time.Date(2100, 12, 31, 23, 59, 59, 999999999, time.UTC),
		SerialNumber:          generateSerial(r),
		Subject: pkix.Name{
			CommonName: "Untrusted Hard Coded Root CA",
		},
	}
	retval.RootCertificate, err = generateParseCertificate(r, rootTemplate, rootTemplate, rootKey.Public(), rootKey)
	if err != nil {
		return CertificateAuthority{}, err
	}

	retval.IntermediateKey, err = ecdsa.GenerateKey(elliptic.P256(), r)
	if err != nil {
		return CertificateAuthority{}, err
	}

	retval.IntermediateCertificate, err = generateParseCertificate(r, &x509.Certificate{
		BasicConstraintsValid: true,
		ExtKeyUsage: []x509.ExtKeyUsage{
			x509.ExtKeyUsageClientAuth,
			x509.ExtKeyUsageServerAuth,
		},
		IsCA:           true,
		KeyUsage:       x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign | x509.KeyUsageCRLSign,
		MaxPathLenZero: true,
		NotBefore:      time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC),
		NotAfter:       time.Date(2100, 12, 31, 23, 59, 59, 999999999, time.UTC),
		SerialNumber:   generateSerial(r),
		Subject: pkix.Name{
			CommonName: "Untrusted Hard Coded Intermediate CA",
		},
	}, retval.RootCertificate, retval.IntermediateKey.Public(), rootKey)
	if err != nil {
		return CertificateAuthority{}, err
	}

	return retval, nil
}

func generateParseCertificate(r io.Reader, template, parent *x509.Certificate, pub, priv any) (*x509.Certificate, error) {
	crt, err := x509.CreateCertificate(r, template, parent, pub, priv)
	if err != nil {
		return nil, err
	}
	return x509.ParseCertificate(crt)
}

func generateSerial(r io.Reader) *big.Int {
	buf := make([]byte, 16)
	if _, err := r.Read(buf); err != nil {
		panic(err)
	}
	ret := big.NewInt(0)
	ret.SetBytes(buf)
	return ret
}
