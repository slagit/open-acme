# Open ACME

A simple ACME Certificate Authority that does *no* verification before issuing a certificate.

**WARNING: THIS CERTIFICATE AUTHORITY IS NOT SAFE FOR PRODUCTION USE**

To assist with developing NixOS configurations that use an ACME
certificate authority, this CA will generate certificates signed by a
hard-coded root CA. Because there is not verification, and the CA private
key is well known, the certificates issued by this CA cannot be trusted
for production use.

## Installation

Installation on a Flakes-based NixOS system is simple:

```flake
{
  inputs.open-acme.url = "gitlab:slagit/open-acme";
  outputs = { self, nixpkgs, open-acme }: {
    nixosConfigurations.my-hostname = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [
        open-acme.nixosModules.open-acme
        ./configuration.nix
      ];
    };
  };
}
```

This will cause ACME certificate requests to use the `open-acme` server
by default.
